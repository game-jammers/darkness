extends Node2D

export var duration = 5.0
var m_remain = 0
var m_target = null

#
# public methods ###############################################################
#

func set_target(target):
	m_target = target

#
# lifecycle ####################################################################
#

func _ready():
	m_remain = duration

func _process(delta):
	if m_target:
		position = m_target.position + Vector2(-42,-100)
		
	m_remain -= delta
	if m_remain <= 0:
		queue_free()
