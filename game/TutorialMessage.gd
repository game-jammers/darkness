extends Node2D

export var period := 1.0
export var amplitude := 0.01

var elapsed = 0

func _process(delta):
	elapsed += delta
	position.y += sin(elapsed * period) * amplitude
