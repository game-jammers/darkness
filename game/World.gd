#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Node2D

#
# internal fields #############################################################
#

onready var player = get_tree().get_nodes_in_group("players")[0]

#
# internal methods #############################################################
#

func _get_climb_children(root):
	for child in root.get_children():
		_get_climb_children(child)
		if "ClimbableTrigger" in child.name:
			if not child.is_connected('body_entered', player, '_on_climb_enter'):
				child.connect('body_entered', player, '_on_climb_enter')
				child.connect('body_exited', player, '_on_climb_exit')

#
# lifecycle methods ############################################################
#

func _ready():
	_get_climb_children(self)
	pass
