#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Node2D

#
# public fields ###############################################################
#

export(String, FILE, "*.tscn") var winScene

#
# external fields #############################################################
#

onready var camera = $Camera2D
onready var hud = $HUD
onready var player = $Player

export var fadeOutTime = 3;

onready var m_oneShot = preload("res://game/OneShotSfx.tscn")
onready var m_switchSfx = preload("res://audio/player/Switch 01 - Melody 04.mp3")

#
# internal fields #############################################################
#

var m_lightCount = 0;
var m_lightsOff = 0;
var m_wood = 0;

var m_lastHudUpdate = 10

var m_gameEnding = false
var m_gameEndingElapsed = 0

#
# events ######################################################################
#

func _on_light_off(_light):
	m_lightsOff += 1
	if !m_gameEnding && m_lightsOff >= m_lightCount:
		_end_game()
#
# -----------------------------------------------------------------------------
#

func _on_register_light(_light):
	m_lightCount += 1


#
# -----------------------------------------------------------------------------
#

func _on_pickup_wood(_wood):
	m_wood += 1

#
# -----------------------------------------------------------------------------
#

func get_wood():
	return m_wood
	
#
# -----------------------------------------------------------------------------
#

func get_stars_collected():
	return m_lightsOff
	
#
# -----------------------------------------------------------------------------
#

func get_total_stars():
	return m_lightCount
	
#
# -----------------------------------------------------------------------------
#

func _play_sfx(sfx):
	var oneShot = m_oneShot.instance()
	oneShot.position = position
	add_child(oneShot)
	oneShot.volume_db = 2
	oneShot.stream = sfx;
	oneShot.play();

#
# -----------------------------------------------------------------------------
#

func _end_game():
	var fader = $Camera2D/Fader
	$HUD/Music.stop()
	$HUD.visible = false
	$HUD/Canvas/Stars.visible = false
	$HUD/Canvas/Wood.visible = false
	_play_sfx(m_switchSfx)
	fader.visible = true
	fader.z_index = 500
	player.z_index = 1000
	player.go_dark()
	
	m_gameEnding = true
	m_gameEndingElapsed = 0
	
#
# lifecycle ###################################################################
#

func _process(delta):
	m_lastHudUpdate += delta
	if m_lastHudUpdate > 0.5 and hud:
		m_lastHudUpdate = 0.0
		hud.update_hud(m_lightCount, m_lightsOff, m_wood)
		
	#if Input.is_action_just_pressed("cheat_codez"):
	#	_end_game()

	if m_gameEnding:
		m_gameEndingElapsed += delta
		var perc = m_gameEndingElapsed / fadeOutTime;
		if perc >= 1.0:
			var res = get_tree().change_scene("res://game/MainMenu.tscn")
			if res != OK:
				push_error("Not sure what you want me to do this this ")

#
# -----------------------------------------------------------------------------
#

func _physics_process(_delta):
	camera.position = player.position
