#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends AudioStreamPlayer2D

#
# events ######################################################################
#

func _on_finished():
	queue_free()

#
# lifecycle ###################################################################
#

func _ready():
	var res = connect("finished", self, "_on_finished")
	if res:
		push_error(res)	

