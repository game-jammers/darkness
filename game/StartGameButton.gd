extends Container

onready var start_image = preload("res://interface/start_button.png")
onready var hover_image = preload("res://interface/start_button_hover.png")
onready var sprite = $Sprite

func _ready():
	pass # Replace with function body.

func _on_entered():
	sprite.texture = hover_image


func _on_exited():
	sprite.texture = start_image
