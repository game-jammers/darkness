#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Area2D

#
# fields ######################################################################
#

onready var game = get_tree().get_root()
onready var player = get_tree().get_nodes_in_group("players")[0]

#
# #############################################################################
#




func _on_body_entered(body):
	if body == player:
		var res = get_tree().change_scene("res://game/Game.tscn")
		if res != OK:
			push_error("Not sure what you want me to do this this ")
