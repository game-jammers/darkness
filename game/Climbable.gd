#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Area2D

onready var m_player = get_tree().get_nodes_in_group("players")[0]

#
# #############################################################################
#

func _ready():
	var res = connect('body_entered', m_player, '_on_climb_enter')
	res = connect('body_exited', m_player, '_on_climb_exit')
	if res:
		push_error(res)

