#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Node2D

#
# fields #######################################################################
#

export var changeSceneDelay = 3
export(String, FILE, "*.tscn") var starFile

onready var fader = $Control/CanvasLayer/Fader
onready var player = $Camera2D/PlayerCutscene
onready var camera = $Camera2D
onready var starPrefab = load(starFile)

var m_nextStar = 0
var m_stars = []

var m_changingScene = false
var m_changeElapsed = 0

#
# events #######################################################################
#

func _start_game():
	if !m_changingScene:
		m_changingScene = true
		m_changeElapsed = 0
		fader.visible = true

#
# lifecycle ####################################################################
#

func _ready():
	m_nextStar = randf() * 2.5

#
# ------------------------------------------------------------------------------
#

func _process(delta):
	#
	# do stars
	#
	m_nextStar -= delta
	if m_nextStar <= 0:
		m_nextStar = randf() * 10 + 5
		var star = starPrefab.instance()
		star.position.x = 300
		star.position.y = randf() * 240 - 120
		camera.add_child(star)
		m_stars.append(star)
	
	#
	# move camera and player and stars
	#
	camera.position.x += 100 * delta;
	player.position.y += sin(camera.position.x / 50 ) * 0.4
	for star in m_stars:
		star.position.x -= (randf()*25.0 + 25.0) * delta
		star.position.y += sin(camera.position.x / 100) * 0.1
		star.z_index = -1
		
		var scale = sin(camera.position.x / 150) * 0.15 + 0.75
		star.scale.x = scale
		star.scale.y = scale
		if star.position.x < -500:
			m_stars.erase(star)
			star.queue_free()
	
	#
	# handle scene transition
	#
	if m_changingScene:
		m_changeElapsed += delta
		var perc = m_changeElapsed / changeSceneDelay
		fader.modulate.a = perc - 0.1
		if perc >= 1.0:
			get_tree().change_scene("res://game/Game.tscn")

func _on_Button_gui_input(event):
	var mouse_event = event as InputEventMouseButton
	if mouse_event and mouse_event.button_index == 1: 
		_start_game()
