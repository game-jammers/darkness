#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Node2D

#
# #############################################################################
#

onready var m_game = get_tree().get_nodes_in_group("game")[0]

onready var m_ladderS = preload("res://game/deployable/Ladder00.tscn")
onready var m_ladder00 = preload("res://game/deployable/Ladder01.tscn")
onready var m_ladder01 = preload("res://game/deployable/Ladder02.tscn")
onready var m_ladder02 = preload("res://game/deployable/Ladder03.tscn")
onready var m_ladderE = preload("res://game/deployable/Ladder04.tscn")

onready var m_bridgeS  = preload("res://game/deployable/Bridge00.tscn")
onready var m_bridge00 = preload("res://game/deployable/Bridge01.tscn")
onready var m_bridge01 = preload("res://game/deployable/Bridge02.tscn")
onready var m_bridge02 = preload("res://game/deployable/Bridge03.tscn")
onready var m_bridgeE  = preload("res://game/deployable/Bridge04.tscn")

var m_deployed = false
var m_validPlacement = false

var m_wood = 0

var m_is_bridge = false

#
# #############################################################################
#

func update_trial(_player):

	# step 1, find the ground
	var space_state = get_world_2d().direct_space_state
	var ignore = [self] + get_children()
	var result = space_state.intersect_ray(position, position + Vector2(0,32), ignore)

	m_validPlacement = false
	var shouldBeBridge = false
	if m_game.get_wood() > 0 && _player.is_on_floor():
		m_validPlacement = true
		if result:
			position.y = result.position.y
			shouldBeBridge = false
		else:
			shouldBeBridge = true

	# scale, and create tiles if necessary
	scale.x = _player.get_facing()
	if shouldBeBridge && !m_is_bridge:
		_destroy_sprites()
		_build_bridge()
	elif !shouldBeBridge && m_is_bridge:
		_destroy_sprites()
		_build_ladder()

	modulate.a = 0.8
	if m_validPlacement:
		modulate.r = 0
		modulate.g = 1
		modulate.b = 1
	else:
		modulate.r = 1
		modulate.g = 0
		modulate.b = 0


#
# -----------------------------------------------------------------------------
#

func deploy(player):
	if m_validPlacement:
		m_deployed = true
		modulate.r = 1
		modulate.g = 1
		modulate.b = 1
		modulate.a = 1
		
	else:
		destroy()

	
#
# -----------------------------------------------------------------------------
#

func is_deployed():
	return m_deployed

#
# -----------------------------------------------------------------------------
#

func is_valid_placement():
	return m_validPlacement;

#
# -----------------------------------------------------------------------------
#

func destroy():
	for child in get_children():
		remove_child(child)
		child.queue_free()
	queue_free()

#
# #############################################################################
#

func _ready():
	m_is_bridge = false
	_build_ladder()

#
# #############################################################################
#

func _destroy_sprites():
	for child in get_children():
		if child is Area2D || child is StaticBody2D:
			remove_child(child)
			child.queue_free()

#
# -----------------------------------------------------------------------------
#

func _build_ladder():
	var startLadder = m_ladderS.instance()
	add_child(startLadder)
	startLadder.position.x = 0
	startLadder.position.y = 0
	
	var ladderParts = [m_ladder00, m_ladder01, m_ladder02]
	var wood = m_game.get_wood()
	for idx in range(0, wood-1):
		var ladderPiece = ladderParts[randi() % ladderParts.size()]
		var newLadder = ladderPiece.instance()
		add_child(newLadder)
		newLadder.position.x = 0
		newLadder.position.y = -32 * idx

	var newLadder = m_ladderE.instance()
	add_child(newLadder)
	newLadder.position.x = 0
	newLadder.position.y = -32 * (wood-1)

	m_is_bridge = false

#
# -----------------------------------------------------------------------------
#

func _build_bridge():
	var startBridge = m_bridgeS.instance()
	add_child(startBridge)
	startBridge.position.x = -32 
	startBridge.position.y = 0
	
	var bridgeParts = [m_bridge00, m_bridge01, m_bridge02]
	var wood = m_game.get_wood()
	for idx in range(0, wood-1):
		var bridgePiece = bridgeParts[randi() % bridgeParts.size()]
		var newBridge = bridgePiece.instance()
		add_child(newBridge)
		newBridge.position.x = 32 * idx
		newBridge.position.y = 0
	
	var newBridge = m_bridgeE.instance()
	add_child(newBridge)
	newBridge.position.x = 32 * (wood-1)
	newBridge.position.y = 0
		
	m_is_bridge = true
