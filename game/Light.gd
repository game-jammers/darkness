#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Area2D

#
# fields ######################################################################
#

signal light_touched

onready var game = get_tree().get_nodes_in_group("game")[0]
onready var player = get_tree().get_nodes_in_group("players")[0]
onready var sprite = $Sprite
onready var offSprite = $OffSprite
onready var light = $Light2D
onready var sfx = $AudioStreamPlayer2D

export(String, FILE, "*.mp3") var pickupSfx

export var flyAwayTime = 5.0

var m_pickedUp = false
var m_elapsed = 0

#
# events #######################################################################
#
 
func _on_entered(body):
	if !m_pickedUp && body == player:
		_pick_up()
		
#
# internal methods #############################################################
#

func _pick_up():
	m_pickedUp = true

	sprite.modulate.a = 0
	offSprite.modulate.a = 1
	light.enabled = false

	emit_signal('light_touched')
	game._on_light_off(self)
	sfx.play()

#
# lifecycle ###################################################################
#

func _ready():
	game._on_register_light(self)
	m_pickedUp = false
	sfx.stream = load(pickupSfx)
	sprite.modulate.a = 1
	offSprite.modulate.a = 0

#
# -----------------------------------------------------------------------------
#

func _process(dt):
	if m_pickedUp && flyAwayTime >= 0 :
		m_elapsed += dt	
		var perc = m_elapsed / flyAwayTime
		if perc >= 1.0:
			queue_free()
		else:

			position.y -= 100.0 * perc * dt;
			position.x += 10.0 * sin(perc) * dt;
