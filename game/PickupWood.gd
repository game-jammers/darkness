#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Area2D

#
# fields ######################################################################
#

onready var game = get_tree().get_nodes_in_group("game")[0]
onready var player = get_tree().get_nodes_in_group("players")[0]
onready var sfx = $AudioStreamPlayer2D
onready var sprite = $Sprite

export(String, FILE, "*.mp3") var pickupSfx

export var flyAwayTime = 5.0

var m_pickedUp = false;
var m_elapsed = 0

#
# events ######################################################################
#

func _on_entered(_body):
	print('on entered')
	if !m_pickedUp && _body == player:
		print('was player')
		m_pickedUp = true;
		sfx.play();
		game._on_pickup_wood(self)

#
# lifecycle ###################################################################
#

func _ready():
	m_pickedUp = false
	sfx.stream = load(pickupSfx)

#
# #############################################################################
#

func _process(dt):
	if m_pickedUp:
		var perc = m_elapsed / flyAwayTime
		if perc >= 1:
			queue_free()
		else:
			m_elapsed += dt
			sprite.position.y -= 50 * perc * dt
			sprite.modulate.a = 1.0 - perc - 0.2;
