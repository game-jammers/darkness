#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends "res://game/Light.gd"

var dialogBox = preload("res://game/DialogBox.tscn")

#
# fields #######################################################################
#

#
# Light methods ################################################################
#

func _pick_up():
	if game.get_stars_collected() == game.get_total_stars() - 1:
		._pick_up()
	else:
		var ndlg = dialogBox.instance()
		game.add_child(ndlg)
		ndlg.set_target(player)
