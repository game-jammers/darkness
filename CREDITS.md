
# Prototyping Assets Used

## Parallax Backgrounds

- Mountain at Dusk Backgrounds by ansimuz https://opengameart.org/content/mountain-at-dusk-background
- Kenney.nl 1-Bit Pack by kenney https://kenney.nl/assets/bit-pack
- Candles with Eyes by vk https://opengameart.org/content/candles-oil-lamp-and-eye-with-varied-stands
