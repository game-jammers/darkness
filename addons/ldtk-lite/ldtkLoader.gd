#
# (c) GameJammers 2022
# http://www.jamming.games
#

tool
extends EditorImportPlugin

#
# fields ######################################################################
#

var LDtkProject = preload("ldtkProject.gd").new();

#
# EditorImportPlugin ##########################################################
#

func get_importer_name():
	return "ldtk.loader"

#
# -----------------------------------------------------------------------------
# 

func get_visible_name():
	return "LDtk Project"

#
# -----------------------------------------------------------------------------
#

func get_priority():
	return 1

#
# -----------------------------------------------------------------------------
#

func get_resource_type():
	return "PackedScene"

#
# -----------------------------------------------------------------------------
#

func get_recognized_extensions():
	return ["ldtk"]

#
# -----------------------------------------------------------------------------
#

func get_save_extension():
	return "tscn"

#
# -----------------------------------------------------------------------------
#

func get_import_options(preset):
	return [
	]

#
# -----------------------------------------------------------------------------
#

func get_option_visibility(option, options):
	return true

#
# -----------------------------------------------------------------------------
#

func get_preset_count():
	return 1

#
# -----------------------------------------------------------------------------
#

func get_preset_name(preset):
	return "Default"

#
# -----------------------------------------------------------------------------
#

func import(source_file, save_path, options, platform_variants, gen_files):
	#
	# load the json data
	#
	var proj = LDtkProject.load_project(source_file)

	#
	# load the tilesets
	#
	var tilesets = LDtkProject.create_tilesets(proj)
	for uid in tilesets:
		var tileset = tilesets[uid]
		var path = 'res://%s.res' % [tileset['name']]
		print('saving %s(%d) to %s' % [tileset['name'], tileset['uid'], path])
		ResourceSaver.save(path, tileset['tileset'])

	#
	# load the levels
	#
	var levels = LDtkProject.create_levels(proj, tilesets)
	_set_owner_recursive(levels, levels)
	
	#
	# prep the scene
	#
	var scene = PackedScene.new()
	scene.pack(levels)

	#
	# write the scene
	#
	var filename = "%s.%s" % [save_path, get_save_extension()]
	return ResourceSaver.save(filename, scene)

#
# #############################################################################
#

func _set_owner_recursive(node, root):
	for child in node.get_children():
		child.set_owner(root)
		_set_owner_recursive(child, root)
	
