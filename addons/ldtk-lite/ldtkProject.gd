#
# (c) GameJammers 2022
# http://www.jamming.games
#

tool
extends Reference

#
# fields ######################################################################
#

#
# internal methods ############################################################
#

#
# public api ##################################################################
#

func load_project(filepath):
	if not filepath is String:
		return null 

	var json_file = File.new()
	json_file.open(filepath, File.READ)
	var json = JSON.parse(json_file.get_as_text()).result
	json_file.close()
 
	json['working_dir'] = filepath.get_base_dir()
	return json

#
# tilesets ////////////////////////////////////////////////////////////////////
#

func create_tilesets(proj): 
	var result = {}
	for tilesetData in proj.defs.tilesets:
		var tileset = create_tileset(proj, tilesetData)
		if tileset != null:
			result[tileset["uid"]] = tileset

	return result

#
# -----------------------------------------------------------------------------
#

func create_tileset(proj, data):
	if proj == null || data == null || data.relPath == null:
		return null
		
	var tileset = TileSet.new()
	var texture_path = '%s/%s' % [proj.working_dir, data.relPath]
	var texture = load(texture_path)
	var texdata = texture.get_data()

	var cellw = (data.pxWid - data.padding) / (data.tileGridSize + data.spacing)
	var cellh = (data.pxHei - data.padding) / (data.tileGridSize + data.spacing)
	var celln = cellw * cellh

	var colliders = []
	var oneway = []
	var occlude = []
	for tag in data.enumTags:
		match tag.enumValueId:
			'Collide':
				colliders += tag.tileIds
			'OneDirection':
				oneway += tag.tileIds
			'Occlude':
				occlude += tag.tileIds

	var custom_data = {}
	for cdata in data.customData:
		custom_data[int(cdata.tileId)] = JSON.parse(cdata.data).result
	
	for tile_idx in range(0, celln):
		var tile_rect = _get_tile_rect(tile_idx, data.tileGridSize, data.pxWid, data.padding, data.spacing)
		var tile_img = texdata.get_rect(tile_rect)
		if not tile_img.is_invisible():
			tileset.create_tile(tile_idx)
			tileset.tile_set_tile_mode(tile_idx, TileSet.SINGLE_TILE)
			tileset.tile_set_texture(tile_idx, texture)
			tileset.tile_set_region(tile_idx, tile_rect)

			# add collision and set one way flag if tile is tagged
			var iscol = tile_idx in colliders
			var isone = tile_idx in oneway
			if iscol || isone:
				var cshape = null
				if custom_data.has(tile_idx):
					print(custom_data[tile_idx].collision)
					cshape = _create_collision_shape_from_points(tile_idx, custom_data[tile_idx].collision)
				else:
					cshape = _create_collision_shape(tile_idx, data.tileGridSize)
				tileset.tile_add_shape(tile_idx, cshape, Transform2D(), isone)
							
			# add occluder if tile is tagged
			if tile_idx in occlude:
				tileset.tile_set_light_occluder(tile_idx, _create_occluder_polygon(tile_idx, data))

	return {
		"name": data.identifier,
		"uid": data.uid,
		"tileset": tileset
	}

#
# levels //////////////////////////////////////////////////////////////////////
#

func create_levels(proj, tilesets):
	var root = Node2D.new()
	root.name = "World"
	for level_data in proj.levels:
		var level = create_level(proj, tilesets, level_data, root)
		if level != null:
			root.add_child(level)

	return root

#
# -----------------------------------------------------------------------------
#

func create_level(proj, tilesets, level_data, root):
	var level = Node2D.new()
	level.name = level_data.identifier
	var idx = 0

	var layers = []
	for layer_data in level_data.layerInstances:
		var layer = null
		match layer_data.__type:

			'Entities':
				layer = create_entity_layer(proj, level_data, layer_data, root)

			'Tiles', 'IntGrid':
				layer = create_tile_layer(proj, tilesets, level_data, layer_data, root)

		if layer != null:
			print('z_index = ', layer.z_index)
			layers.append(layer)

	layers.invert()
	for layer in layers:
		level.add_child(layer)

	return level

#
# -----------------------------------------------------------------------------
#

func create_entity_layer(proj, level_data, layer_data, root):
	var layer = Node2D.new()
	for entityData in layer_data.entityInstances:
		match entityData.__identifier:
			'StaticBody2D':
				var entity = StaticBody2D.new()
				entity.position.x = level_data.worldX + entityData.px[0];
				entity.position.y = level_data.worldY + entityData.px[1];
				layer.add_child(entity)
				var cldr = CollisionShape2D.new()
				entity.add_child(cldr)
				cldr.shape = RectangleShape2D.new()
				cldr.shape.extents.x = entityData.__pivot[0] * entityData.width
				cldr.shape.extents.y = entityData.__pivot[1] * entityData.height
			'Climbable':
				var climb = Area2D.new()
				climb.add_to_group("climb")
				climb.name = "ClimbableTrigger"
				climb.position.x = level_data.worldX + entityData.px[0];
				climb.position.y = level_data.worldY + entityData.px[1];
				var cldr = CollisionShape2D.new()
				cldr.shape = RectangleShape2D.new()
				cldr.shape.extents.x = entityData.__pivot[0] * entityData.width
				cldr.shape.extents.y = entityData.__pivot[1] * entityData.height
				climb.add_child(cldr)
				layer.add_child(climb)

			_:
				print("WARNING: entity type %s not yet supported" % [entityData.__identifier])
	
	return layer 

#
# -----------------------------------------------------------------------------
#

func create_tile_layer(proj, tilesets, level_data, layer_data, root):
	if not layer_data.__tilesetDefUid in tilesets:
		return null

	var tsetdata = _get_tileset_data_by_uid(proj, layer_data.__tilesetDefUid)
	var climbable = []
	for tags in tsetdata.enumTags:
		if tags.enumValueId == 'Climbable':
			climbable += tags.tileIds

	var tileset = tilesets[ layer_data.__tilesetDefUid ]['tileset']
	var tilemap = TileMap.new()
	tilemap.tile_set = tileset
	tilemap.name = layer_data.__identifier
	tilemap.position = Vector2(level_data.worldX, level_data.worldY)
	tilemap.cell_size = Vector2(layer_data.__gridSize, layer_data.__gridSize)
	tilemap.modulate = Color(1,1,1,layer_data.__opacity)

	match layer_data.__type:
		'Tiles':
			for tile in layer_data.gridTiles:
				var idx = tile.t
				var grid_pos = _idx_to_grid(tile.d[0], layer_data.__gridSize, level_data.pxWid)

				if idx in climbable:
					var climb = Area2D.new()
					climb.add_to_group("climb")
					climb.name = "ClimbableTrigger"
					var px_pos = _grid_to_px(grid_pos, tsetdata.tileGridSize, tsetdata.padding, tsetdata.spacing)
					climb.position.x = px_pos.x + tsetdata.tileGridSize / 2.0
					climb.position.y = px_pos.y + tsetdata.tileGridSize / 2.0
					var cldr = CollisionShape2D.new()
					cldr.shape = RectangleShape2D.new()
					cldr.shape.extents.x = tsetdata.tileGridSize / 2.0
					cldr.shape.extents.y = tsetdata.tileGridSize / 2.0
					climb.add_child(cldr)
					tilemap.add_child(climb)

				var flip = int(tile['f'])
				var flipX = bool(flip & 1)
				var flipY = bool(flip & 2)
				tilemap.set_cellv(grid_pos, idx, flipX, flipY)

		'IntGrid', 'AutoLayer':
			for tile in layer_data.autoLayerTiles:
				var idx = tile.t
				var dst_pos = tilemap.world_to_map(Vector2(tile.px[0], tile.px[1]));
				if idx in climbable:
					var climb = Area2D.new()
					climb.add_to_group("climb")
					climb.name = "ClimbableTrigger"
					var px_pos = Vector2(tile.px[0], tile.px[1])#_grid_to_px(grid_pos, tsetdata.tileGridSize, tsetdata.padding, tsetdata.spacing)
					climb.position.x = px_pos.x + tsetdata.tileGridSize / 2.0
					climb.position.y = px_pos.y + tsetdata.tileGridSize / 2.0
					var cldr = CollisionShape2D.new()
					cldr.shape = RectangleShape2D.new()
					cldr.shape.extents.x = tsetdata.tileGridSize / 2.0
					cldr.shape.extents.y = tsetdata.tileGridSize / 2.0
					climb.add_child(cldr)
					tilemap.add_child(climb)

				var flip = int(tile['f'])
				var flipX = bool(flip & 1)
				var flipY = bool(flip & 2)
				tilemap.set_cellv(dst_pos, idx, flipX, flipY)

	return tilemap

#
# internal methods ############################################################
#

func _get_tileset_data_by_uid(proj, uid):
	for tsetdata in proj.defs.tilesets:
		if tsetdata.uid == uid:
			return tsetdata
	return null

#
# -----------------------------------------------------------------------------
#

func _create_occluder_polygon(idx, data):
	var rect = _get_tile_rect(idx, data.tileGridSize, data.pxWid, data.padding, data.spacing)
	var poly = OccluderPolygon2D.new()

	poly.set_polygon(PoolVector2Array([
		Vector2(rect.size.x, 0),
		rect.size,
		Vector2(0, rect.size.y),
		Vector2.ZERO
	]))

	return poly

#
# -----------------------------------------------------------------------------
#

func _create_collision_shape(idx, tileGridSize):
	var shape = ConvexPolygonShape2D.new()
	shape.set_points(PoolVector2Array([
		Vector2(tileGridSize, 0), # top right
		Vector2(tileGridSize, tileGridSize), # bottom right
		Vector2(0, tileGridSize), #bottom left
		Vector2.ZERO #top left
	]))
	return shape

#
# ------------------------------------------------------------------------------
#

func _create_collision_shape_from_points(idx, points):
	var shape = ConvexPolygonShape2D.new()
	var ps = []
	for point in points:
		ps.append(Vector2(point[0], point[1]))
	shape.set_points(PoolVector2Array(ps))
	return shape

#
# -----------------------------------------------------------------------------
#

func _get_tile_rect(idx, tileGridSize, pxWid, padding, spacing):
	var grid = _idx_to_grid(idx, tileGridSize, pxWid)
	var pos = _grid_to_px(grid, tileGridSize, padding, spacing)
	return Rect2(pos, Vector2(tileGridSize, tileGridSize))
#
# -----------------------------------------------------------------------------
#

func _idx_to_grid(idx, tileGridSize, pxWid):
	var grid_based_width = floor(pxWid / tileGridSize)
	var gy = floor(idx / grid_based_width)
	var gx = idx - gy * grid_based_width;
	return Vector2(gx,gy)
	
#
# -----------------------------------------------------------------------------
#

func _grid_to_px(grid, tileGridSize, padding, spacing):
	var px = padding + grid.x * ( tileGridSize + spacing )
	var py = padding + grid.y * ( tileGridSize + spacing )
	return Vector2(px,py);
