#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends Control

#
# fields ######################################################################
#

onready var maxCoinLabel = get_node("Canvas/Stars/MaxStars")
onready var curCoinLabel = get_node("Canvas/Stars/CurStars")
onready var curWoodLabel = get_node("Canvas/Wood/Wood")
onready var fader = get_node("Canvas/Fader")

#
# events ######################################################################
#

func update_hud(totalLight, offLights, woodCount):
	maxCoinLabel.text = String(totalLight)
	curCoinLabel.text = String(offLights)
	curWoodLabel.text = String(woodCount)

#
# -----------------------------------------------------------------------------
#

func set_fader(perc):
	fader.visible = true
	fader.modulate.a = perc;
