#
# (c) GameJammers 2022
# http://www.jamming.games
#

extends KinematicBody2D

#
# constants ###################################################################
#

const UP = Vector2(0,-1)

#
# fields ######################################################################
#

onready var m_game = get_tree().get_nodes_in_group("game")[0]

onready var m_animSprite = $Sprite
onready var m_collider = $Collider

onready var m_oneShot = preload("res://game/OneShotSfx.tscn")
onready var m_jumpSfx = preload("res://audio/player/Jump 1.mp3")
onready var m_fallSfx = preload("res://audio//Silence.mp3")
onready var m_landHardSfx = preload("res://audio/player/Land (Hard).mp3")
onready var m_landSoftSfx = preload("res://audio/player/Land (Soft).mp3")
onready var m_footstepSfx = preload("res://audio/player/footsteps/Footsteps - Snow.mp3")
onready var m_climbDownSfx = preload("res://audio/player/ladder/Ladder Down.mp3")
onready var m_climbUpSfx = preload("res://audio/player/ladder/Ladder Up.mp3")
onready var m_buildSfx = preload("res://audio/player/Wood_Build.mp3")

onready var m_ladder = preload("res://game/Deployable.tscn")

#
# inspector ###################################################################
#

export var moveSpeed := 70.0
export var fallSpeed := 500.0
export var timeToTerminalVelo := 2.0
export var jumpSpeed := 200
export var jumpTime := 0.5
export var buildTime := 1.0

#
# internal --------------------------------------------------------------------
#

var m_velocity = Vector2(0,0)
var m_impulse = Vector2(0,0)
var m_facing = 1

var m_jump = false
var m_jumpElapsed = 0

var m_isFalling = false
var m_fallElapsed = 0
var m_airElapsed = 0

var m_is_on_floor = false
var m_is_on_ceiling = false

var m_is_climbing = false
var m_can_climb = 0

var m_sfxSrc = null
var m_is_fading = false
var m_fadeOut = 0

var m_activeDeployable = null
var m_is_building = false
var m_buildingElapsed = 0

var m_dark = false

#
# internal methods ############################################################
#

func _play_sfx(sfx):
	var oneShot = m_oneShot.instance()
	oneShot.position = position
	m_game.add_child(oneShot)
	oneShot.stream = sfx;
	oneShot.play();
	
#
# ------------------------------------------------------------------------------
#

func _start_walk_sfx():
	if m_sfxSrc.stream != m_footstepSfx or !m_sfxSrc.is_playing():
		_stop_sfx()
		m_sfxSrc.stream = m_footstepSfx
		m_sfxSrc.play()

#
# ------------------------------------------------------------------------------
#

func _start_climb_up_sfx():
	if m_sfxSrc.stream != m_climbUpSfx or !m_sfxSrc.is_playing():
		_stop_sfx()
		m_sfxSrc.stream = m_climbUpSfx;
		m_sfxSrc.play()

#
# ------------------------------------------------------------------------------
#

func _start_climb_down_sfx():
	if m_sfxSrc.stream != m_climbDownSfx or !m_sfxSrc.is_playing():
		_stop_sfx()
		m_sfxSrc.stream = m_climbDownSfx;
		m_sfxSrc.play()

#
# ------------------------------------------------------------------------------
#

func _stop_sfx():
	m_sfxSrc.stop()
	m_is_fading = false
	m_sfxSrc.stream = null

#
# ------------------------------------------------------------------------------
#

func _update_sfx(_delta):
	pass

#
# ------------------------------------------------------------------------------
#

func _can_climb():
	return m_can_climb > 0

#
# ------------------------------------------------------------------------------
#

func _land():
	if m_is_on_floor == false:
		m_is_on_floor = true
		m_isFalling = false
		
		if abs(m_impulse.x) < 0.001:
			if m_fallElapsed > 1:
				_play_sfx(m_landHardSfx)
			else:
				_play_sfx(m_landSoftSfx)
	m_fallElapsed = 0
	m_airElapsed = 0
	
#
# -----------------------------------------------------------------------------
#

func _start_fall():
	if !m_isFalling && m_fallElapsed > 0.1:
		m_isFalling = true
		_play_sfx(m_fallSfx)

#
# callback methods #############################################################
#

func _on_climb_enter(_self):
	if self == _self:
		m_can_climb += 1

#
# ------------------------------------------------------------------------------
#

func _on_climb_exit(_self):
	if self == _self:
		m_can_climb -= 1

#
# actions #####################################################################
#

func is_jumping():
	return m_jump && m_jumpElapsed < jumpTime

#
# -----------------------------------------------------------------------------
#

func is_climbing():
	return m_is_climbing && _can_climb()

#
# -----------------------------------------------------------------------------
#

func jump():
	if m_is_on_floor || is_climbing() :
		m_isFalling = false
		m_jump = true;
		m_jumpElapsed = 0
		m_fallElapsed = 0
		m_airElapsed = 0
		m_is_climbing = false
		_play_sfx(m_jumpSfx)

#
# -----------------------------------------------------------------------------
#

func climb():
	if _can_climb():
		m_isFalling = false
		m_jump = false
		m_is_climbing = true

#
# -----------------------------------------------------------------------------
#

func get_facing():
	return m_facing
	
#
# ------------------------------------------------------------------------------
#

func go_dark():
	m_dark = true

#
# lifecycle ###################################################################
#

func _ready():
	m_sfxSrc = AudioStreamPlayer2D.new()
	add_child(m_sfxSrc)
	_stop_sfx()
	m_sfxSrc.volume_db = -5

#
# -----------------------------------------------------------------------------
#

func _process(delta):
	_update_sfx(delta)
		
#
# -----------------------------------------------------------------------------
#

func _physics_process(delta):
	if m_dark:
		_stop_sfx()
		m_animSprite.play("Dark")
		return
		
	if is_climbing():
		m_isFalling = false
		if not _can_climb():
			_stop_sfx()
			m_is_climbing = false;
		else:
			m_impulse.y = 0;
			if Input.is_action_pressed("climb_up"):
				m_impulse.y = -moveSpeed;
				_start_climb_up_sfx()
			elif Input.is_action_pressed("climb_down"):
				m_impulse.y = moveSpeed;
				_start_climb_down_sfx()

	elif is_jumping():
		m_isFalling = false
		m_jumpElapsed += delta;
		var perc = m_jumpElapsed / jumpTime;
		m_impulse.y = -jumpSpeed * (1.0 - perc)
		m_impulse.y = max(-fallSpeed, m_impulse.y)
		m_airElapsed += delta
		if perc >= 1.0:
			m_jump = false
	
	else:
		_start_fall()
		m_fallElapsed += delta
		m_airElapsed += delta
		m_impulse.y = fallSpeed * m_fallElapsed / (timeToTerminalVelo + jumpTime)

	if m_is_building:
		m_impulse.x = 0
	else:
		if Input.is_action_pressed("move_left"):
			m_impulse.x -= moveSpeed * 0.3
			m_facing = -1
			m_animSprite.scale.x = m_facing
			if m_is_on_floor:
				_start_walk_sfx()
			elif !m_is_climbing:
				_stop_sfx()
		elif Input.is_action_pressed("move_right"):
			m_impulse.x += moveSpeed * 0.3
			m_facing = 1
			m_animSprite.scale.x = m_facing
			if m_is_on_floor:
				_start_walk_sfx()
			elif !m_is_climbing:
				_stop_sfx()
		else:
			if !m_is_climbing:
				_stop_sfx()
			m_impulse.x *= 0.5
			if m_impulse.x < 0.001:
				m_impulse.x = 0

	m_impulse.x = clamp(m_impulse.x, -moveSpeed, moveSpeed)

	if Input.is_action_just_pressed("jump"):
		var doJump = true
		if Input.is_action_pressed("climb_down"):
			if get_slide_count() > 0:
				var coll = get_slide_collision(0)
				var tmap = coll.collider as TileMap;
				if tmap:
					var grid = tmap.world_to_map(position - tmap.position)
					var tidx = tmap.get_cellv(grid)
					if tidx >= 0:
						var tset = tmap.tile_set
						if tset.tile_get_shape_one_way(tidx, 0):
							position.y += 8
							doJump = false
		if doJump:
			jump()

	if Input.is_action_pressed("climb_up") || Input.is_action_pressed("climb_down"):
		climb()

	var target = lerp(m_velocity, m_impulse, 0.5)
	
	if is_jumping() or is_climbing():
		m_velocity = move_and_slide(target, UP)
	else:
		m_velocity = move_and_slide_with_snap(target, Vector2(0,5), UP)

	m_is_on_ceiling = false
	if is_on_ceiling() && m_jumpElapsed > 0.01:
		m_is_on_ceiling = true
		m_fallElapsed = 0
		m_jump = false

	if is_on_floor():
		_land()
	else:
		m_is_on_floor = false

	_try_build();
	_update_animations(delta)

#
# -----------------------------------------------------------------------------
#

func _try_build():
	if m_game.get_wood() > 0 && Input.is_action_pressed("deploy_ladder") && (m_activeDeployable == null || m_activeDeployable.is_deployed() == false):
		if m_activeDeployable == null:
			m_activeDeployable = m_ladder.instance();
			m_game.add_child(m_activeDeployable)
	
		m_activeDeployable.position.x = position.x + (m_facing * 32);
		m_activeDeployable.position.y = position.y - 16 
		m_activeDeployable.update_trial(self)

	elif !Input.is_action_pressed("deploy_ladder") && m_activeDeployable != null && m_activeDeployable.is_deployed() == false:
		if m_activeDeployable.is_valid_placement():
			m_activeDeployable.deploy(self)
			_play_sfx(m_buildSfx)
			m_is_building = true
		else:
			m_activeDeployable.destroy()
			m_activeDeployable = null

	elif Input.is_action_pressed("deploy_ladder") && m_activeDeployable && m_activeDeployable.is_deployed():
		m_activeDeployable.destroy()
		m_activeDeployable = null

	elif Input.is_action_pressed("recall") && m_activeDeployable != null:
		m_activeDeployable.destroy()
		m_activeDeployable = null

#
# -----------------------------------------------------------------------------
#

func _update_animations(dt):
	
	if m_is_building:
		m_buildingElapsed += dt
		if m_buildingElapsed > buildTime:
			m_is_building = false
			m_buildingElapsed = 0

		m_animSprite.play("Build")

	elif is_climbing():
		m_fallElapsed = 0
		m_animSprite.play("Climb")
	elif m_is_on_floor || (not is_jumping() && m_airElapsed < 0.2):
		m_is_climbing = false
		if abs(m_impulse.x) > 0.001:
			m_animSprite.play("Walk")
		else:
			m_animSprite.play("Idle")
	else:
		m_is_climbing = false
		if m_impulse.y > 0:
			m_animSprite.play("Fall")
		else:
			m_animSprite.play("Jump")

