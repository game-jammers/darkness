tool
extends EditorScript

func _get_climb_children(root, player):
	for child in root.get_children():
		_get_climb_children(child, player)
		if "ClimbableTrigger" in child.name:
			if not child.is_connected('body_entered', player, '_on_climb_enter'):
				child.connect('body_entered', player, '_on_climb_enter')
				child.connect('body_exited', player, '_on_climb_exit')

func _run():
	var game = get_editor_interface().get_selection().get_selected_nodes()[0]
	var player = game.get_node("Player");
	_get_climb_children(game, player)
